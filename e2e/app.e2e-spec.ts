import { PortalDashboardV2Page } from './app.po';

describe('portal-dashboard-v2 App', () => {
  let page: PortalDashboardV2Page;

  beforeEach(() => {
    page = new PortalDashboardV2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
