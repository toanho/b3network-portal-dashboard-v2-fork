import { Injectable } from "@angular/core";
import { CookieService } from "../../lib/services/services";

@Injectable()

export class RouterService {
    orgId: string;
    newOrgId: string;
    urlParams = {};
    sessionToken: string;

    constructor() {}

    init() {
        this.initUrlParameter();
        this.orgId = this.urlParams['orgId'] || 'AA-329040';
        this.newOrgId = this.urlParams['orgUuid'] || '6d1453ec-3caa-4df0-8f2b-6b201b0352d7';
        this.sessionToken = this.urlParams['sessionToken'];
    }

    private initUrlParameter() {
        var match,
            pl = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query = window.location.search.substring(1);
        while (match = search.exec(query)) {
            this.urlParams[decode(match[1])] = decode(match[2]);
        }
    }

}

