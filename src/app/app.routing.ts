import { Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomepageComponent } from "./homepage/homepage.component";

export const routes: Routes = [
    {path: "", component: HomepageComponent, pathMatch: "full"},
    {path: "homepage", component: HomepageComponent},
];
export const appRouting = RouterModule.forRoot(routes, { useHash: true });