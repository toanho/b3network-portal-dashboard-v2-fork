import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { RouterService } from '../../shared/services/router.service';
import { OrganizationService, BalanceService, EbService, IdentityService } from '../../lib/services/services';
import { GetMemberResponseV2, GetCompanyInfoResponse, Wallet, 
        Profile, ProfileOrg } from '../../lib/models/api.model';
import {SuccessModalService} from '../../lib/component/general/services';

declare var X: any;

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  private walletUuid: string;

  isLoading = true;
  navItems: NavigationItemModel[] = [];
  orgName: string;

  isLoadingAvailableCredits: boolean;
  availableBalance: number;
  walletCurrency = new Wallet().currency;

  profile: Profile = new Profile();

  constructor(private routerService: RouterService, private orgService: OrganizationService,
              private balanceService: BalanceService, private identityService: IdentityService,
              private successModalService: SuccessModalService) { }

  ngOnInit() {
    this.initProfile();
    this.initNavItems();
    this.getOrgInfoAndMemberList();
    this.registerEventListener();
  }

  initProfile() {
    this.identityService.getProfile(this.routerService.sessionToken).subscribe((response:Profile) => {
			this.profile = new Profile(response);
			this.profile.organizations = this.profile.organizations.filter((org:ProfileOrg) => {
				return org.orgUuid == this.routerService.newOrgId;
			});
			if (this.profile.organizations.length == 0) {
				this.profile = new Profile();
				X.showWarn('You not belong to this organization anymore.');
			}
		}, error => {
			X.showWarn('Sorry, an error has occurred when we try to fulfill your request. Please try again in a few minutes.');
		});
  }

  registerEventListener() {
    X.addPaymentChangeEventListener(() => {
      this.getAvailableCredit(this.walletUuid);
    });

    X.addMemberChangeEventListener(() => {
      this.getOrgInfoAndMemberList();
    });

    X.addMemberSuccessEventListener((data) => {
      this.successModalService.openSuccessModal(`<strong>${data.name}</strong> has been successfully added to your organization`);
      this.getOrgInfoAndMemberList();
    });

    X.addCompanyChangeEventListener(() => {
      this.getOrgInfoAndMemberList();
    });
  }

  getAvailableCredit(walletID: string) {
    this.isLoadingAvailableCredits = true;
    this.balanceService.getBalance(this.routerService.newOrgId, this.routerService.sessionToken).subscribe((res: Wallet) => {
      this.availableBalance = res.balance.availableForReservation;
      this.walletCurrency = res.currency;
      this.isLoadingAvailableCredits = false;
      this.isLoading = false;
    }, error => {
      this.isLoadingAvailableCredits = false;
      this.isLoading = false;
    });
  }

  getOrgInfoAndMemberList() {
    Observable.forkJoin(
      this.orgService.getMemberListV2(this.routerService.newOrgId, false, this.routerService.sessionToken),
      this.orgService.getCompanyInfo(this.routerService.newOrgId, this.routerService.sessionToken)
    ).subscribe(
      data => {
        let memberList = <GetMemberResponseV2[]>data[0];
        this.fetchMembersData(memberList);

        let orgInfo = <GetCompanyInfoResponse>data[1];

        if (orgInfo) {
          this.orgName = orgInfo.name;

          if (orgInfo.walletUuid) {
            this.walletUuid = orgInfo.walletUuid;
            this.getAvailableCredit(orgInfo.walletUuid);
          } else {
            this.isLoading = false;
          }
        } else {
          this.isLoading = false;
        }
      }, err => {
        this.isLoading = false;
        X.showWarn('Sorry, an error has occurred when we try to fulfill your request. Please try again in a few minutes.');
      });
  }

  fetchMembersData(members: GetMemberResponseV2[]) {
    if (members) {
      let owner_no = members.filter(x => x.role === "OWNER").length;
      let admin_no = members.filter(x => x.role === "ADMIN").length;
      let member_no = members.filter(x => x.role === "MEMBER").length;
      let memberDescription = this.setMembersBusinessCases(this.setMembersDataText(owner_no, 'owner'),
        this.setMembersDataText(admin_no, 'admin'),
        this.setMembersDataText(member_no, 'active member'));
      let memberNavItem = this.navItems.find((item: NavigationItemModel) => {
        return item.id == 1;
      });
      memberNavItem.description = memberDescription;
    }
  }

  setMembersDataText(number, text) {
    let dataReturned: string; 
       
    if (number == 0) {
      dataReturned = "";
    } else if (number == 1) {
      dataReturned = `${number} ${text}`;
    } else {
      dataReturned = `${number} ${text}s`;
    }

    return dataReturned;
  }

  setMembersBusinessCases(owner_data, admin_data, member_data) {
    let dtReturned;

    if (owner_data != '' && admin_data != '' && member_data != '') {
      dtReturned = `${owner_data}, ${admin_data} and ${member_data}`;
    } else if (owner_data == '') {
      if (admin_data == '' || member_data == '') {
        dtReturned = `${admin_data} ${member_data}`
      } else {
        dtReturned = `${admin_data} and ${member_data}`
      }
    } else if (admin_data == '') {
      if (owner_data == '' || member_data == '') {
        dtReturned = `${owner_data} ${member_data}`
      } else {
        dtReturned = `${owner_data} and ${member_data}`
      }
    } else if (member_data == '') {
      if (owner_data == '' || admin_data == '') {
        dtReturned = `${owner_data} ${admin_data}`
      } else {
        dtReturned = `${owner_data} and ${admin_data}`
      }
    }
    
    return dtReturned;
  }

  directToPage(directURL: string) {
    window.top.location.href = `${document.referrer}${directURL}`;
  }

  initNavItems() {
    this.navItems = [
      {
        id: 1,
        name: 'MEMBERS',
        directURL: '#/member',
        description: '',
        imageUrl: 'https://uikit.b3networks.com/libs/icon-branding/ico-user.svg',
        imageAlt: 'Members'
      },
      {
        id: 3,
        name: 'PAYMENT',
        directURL: '#/payment',
        description: 'Manage payment method, top-up credits',
        imageUrl: 'https://uikit.b3networks.com/libs/icon-branding/ico-payment.svg',
        imageAlt: 'Payment',
      },
      {
        id: 5,
        name: 'SETTINGS',
        directURL: '#/organizationSettings',
        description: 'Manage your organization profile, billing information, and logo',
        imageUrl: 'https://uikit.b3networks.com/libs/icon-branding/ico-configure.svg',
        imageAlt: 'Settings'
      },
    ];
  }

  isRoleMember() {
    return this.profile.organizations[0].isMember();
  }

  isHideItem(item: NavigationItemModel): boolean {
    return (this.isRoleMember() && (item.name == 'PAYMENT' || item.name == 'SETTINGS'));

  }
}

export class NavigationItemModel {
  id: number;
  name: string;
  directURL: string;
  description: string;
  imageUrl: string;
  imageAlt: string
}
