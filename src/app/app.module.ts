import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
/*
 * Injectables
 */
import { libServicesInjectables } from "../lib/services/services";
import { localServicesInjectables } from '../shared/services/services';
/*
 * App Routing
 */
import { appRouting } from "./app.routing";
/*
 * Components
 */
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { QuicklinksComponent } from './quicklinks/quicklinks.component';

import { GeneralModule } from '../lib/component/general/general.module';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    QuicklinksComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    appRouting,
    GeneralModule
  ],
  providers: [ localServicesInjectables, libServicesInjectables ],
  bootstrap: [AppComponent]
})
export class AppModule { }
