import { Component, Input, OnInit } from '@angular/core';

declare var X: any;

@Component({
  selector: 'app-quicklinks',
  templateUrl: './quicklinks.component.html'
})
export class QuicklinksComponent implements OnInit {
  @Input() role: string;

  quicklinks = QUICKLINKS;

  constructor() { }

  ngOnInit() {
  }

  processQuickLink(eventName: string) {
    switch (eventName) {
      case 'showAddMemberModal':
        X.showAddMemberModal();
        break;
      case 'viewPendingInvite':
        X.triggerViewPendingInviteEvent();
        break;
      case 'viewBilling':
        X.triggerViewBillingInfoEvent();
        break;
      case 'topup':
        X.topup();
        break;
    }
  }

  isRoleMember(): boolean {
		return this.role.toLowerCase() == 'member';
	}

  isHideItem(item: QuicklinkModel): boolean {
    return (this.isRoleMember() && (item.eventName == 'topup' || item.eventName == 'viewBilling'));
  }
}

const QUICKLINKS: QuicklinkModel[] = [
  {
    icon: 'account circle',
    content: 'Add member',
    eventName: 'showAddMemberModal'
  },
  {
    icon: 'settings',
    content: 'Edit Billing Information',
    eventName: 'viewBilling'
  },
  {
    icon: 'attach money',
    content: 'Top Up Credits',
    eventName: 'topup'
  }
];

export class QuicklinkModel {
  icon: string;
  content: string;
  eventName: string;
}