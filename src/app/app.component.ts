import { Component } from '@angular/core';

import { CookieService } from '../lib/services/services';
import { RouterService } from '../shared/services/services';

declare var X: any;

@Component({
  selector: 'app-root',
  template: `
    <success-modal></success-modal>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {
  
  constructor(private routerService: RouterService, private cookieService: CookieService){
    this.init();
  }

  init(){
    X.init();
    this.routerService.init();
  }

}
